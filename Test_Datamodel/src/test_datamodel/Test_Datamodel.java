/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_datamodel;

import api.APIConnection;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import datamodel.*;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Alex
 */
public class Test_Datamodel {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //ModelController.getInstance().initialiseModel();
//        ArrayList<Customer> customerList = ModelController.getInstance().getCustomerList();
//        System.out.println(customerList.get(0).getCustomerId() + " : " +customerList.get(0).getFirstName() + " " + customerList.get(0).getLastName());
//        System.out.print(customerList.get(0).getDateOfBirth().toString());

//        ModelController.getInstance().populateOrderData();
//        ArrayList<Order> orderList = ModelController.getInstance().getOrderList();
//        
//        for (int i = 0; i < orderList.size(); i++) {
//            for (int j = 0; j < orderList.get(i).getOrderItems().size(); j++) {
//                for (int k = 0; k < orderList.get(i).getOrderItems().get(j).getCustomisations().size(); k++) {
//                    System.out.println(orderList.get(i).getOrderItems().get(j).getCustomisations().get(k));
//                }
//            }
//        }

//        ArrayList<Order> orders = new ArrayList<>();
//
//        for (int i = 0; i < 5; i++) {
//            OrderItem item1 = new OrderItem(1, i, "Small", 1, 1, null);
//            OrderItem item2 = new OrderItem(2, 2, "Medium", 2, 2, null);
//            ArrayList<OrderItem> items = new ArrayList<>();
//            items.add(item1);
//            items.add(item2);
//            Order order = new Order(1, 15.99, LocalDateTime.now(), "Order Placed", 1, null, null, items);
//            orders.add(order);
//        }

//            OrderItem item1 = new OrderItem(1, 1, "Small", 1, 1, null);
//            OrderItem item2 = new OrderItem(2, 2, "Medium", 2, 2, null);
//            ArrayList<OrderItem> items = new ArrayList<>();
//            items.add(item1);
//            items.add(item2);
//            Order order = new Order(1, 15.99, LocalDateTime.now(), "Order Placed", 1, null, null, items);
//            
//            System.out.println(APIConnection.postOrderData(order));
//
//        //DefaultIngredientQuantity defaultIngredient = new DefaultIngredientQuantity(3, 2, 3);
//        
//        //System.out.println(APIConnection.getOrderData().get(0).getOrderId());
//        for (int i = 0; i < 5; i++) {
//            System.out.println(APIConnection.postOrderData(orders.get(i)));
//        }

//        Staff staff = new Staff();
//        staff.setPassword("hello");
//        System.out.println(staff.getPassword());
//        SecureRandom random = new SecureRandom();
//        String salt = new BigInteger(130, random).toString(32);
//        System.out.println(salt);
//        System.out.println(Hash.saltAndHashPassword("sirwin", salt));
//        System.out.println(Hash.saltAndHashPassword("sirwin", salt).length());

//        String AESKeyString = "pzVrAZiR2E/XRs8UypWnKA==";
//        String AESIVString = "NYVhj8MWAdtmb6IibuyR1A==";
//        byte[] AESKey;
//        byte[] AESIV;
//        
//        try {
//            AESKey = java.util.Base64.getDecoder().decode(AESKeyString.getBytes("UTF-8"));
//            AESIV = java.util.Base64.getDecoder().decode(AESIVString.getBytes("UTF-8"));
//
//            try {
//                Cipher AesCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//                SecretKey key = new SecretKeySpec(AESKey, 0, AESKey.length, "AES");
//                IvParameterSpec ivParam = new IvParameterSpec(AESIV);
//                System.out.println(Cipher.getMaxAllowedKeyLength("AES/CBC/PKCS5Padding"));
//                
//                AesCipher.init(Cipher.DECRYPT_MODE, key, ivParam);
//                byte[] input = APIConnection.getData("Customer/Encrypted");
//                if (input != null) {
//                    byte[] result = AesCipher.doFinal(input);
//                    System.out.println(result);
//                    System.out.println(new String(result, "UTF-8"));
//                }
//                
//            } catch (NoSuchAlgorithmException ex) {
//                Logger.getLogger(Test_Datamodel.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (NoSuchPaddingException ex) {
//                Logger.getLogger(Test_Datamodel.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (InvalidKeyException ex) {
//                Logger.getLogger(Test_Datamodel.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (InvalidAlgorithmParameterException ex) {
//                Logger.getLogger(Test_Datamodel.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (IllegalBlockSizeException ex) {
//                Logger.getLogger(Test_Datamodel.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (BadPaddingException ex) {
//                Logger.getLogger(Test_Datamodel.class.getName()).log(Level.SEVERE, null, ex);
//            }            
//            
//        } catch (UnsupportedEncodingException ex) {
//            Logger.getLogger(Test_Datamodel.class.getName()).log(Level.SEVERE, null, ex);
//        }     
//        
//        Integer sizeId = ModelController.getInstance().getSizeOptionsList().get(1).getSizeId();
//        DealItem item = new DealItem("type", sizeId, 1);
//        item.setDealItemId(0);

//        ModelController.getInstance().populateSizeOptionsData();
//        
//        for (SizeOptions size : ModelController.getInstance().getSizeOptionsList()) {
//            System.out.println(size.getSizeId());
//        }
        
//        Boolean isSuccessful = APIConnection.staffLogin("sirwin", "srwin");
//        System.out.println(isSuccessful);
//        
//        System.out.println(APIConnection.getStaffSalt("jevans"));

//        ModelController.getInstance().populateSizeOptionsData();
//        ArrayList<SizeOptions> sizes = ModelController.getInstance().getSizeOptionsList();
//        
//        for (SizeOptions size : sizes) {
//            System.out.println(size.getSizeName());
//        }

//        ModelController.getInstance().populateProductData();
//        
//        for (ProductSizes size : ModelController.getInstance().getProductList().get(0).getProductSizes()) {
//            System.out.println(size.getSizeName());
//        }

        ModelController.getInstance().populateProductData();
        
        for (Product product : ModelController.getInstance().getProductList()) {
            if (product.getProductId().equals(16)) {
                for (ProductSizes size : product.getProductSizes()) {
                    System.out.println(size.getSizeId());
                    System.out.println(size.getPrice());
                }
            }
        }
        
//        ModelController.getInstance().populateOrderData();
//        
//        for (Order order : ModelController.getInstance().getOrderList()) {
//            if (order.getOrderId().equals(10)) {
//                for (OrderItem orderItem : order.getOrderItems()) {
//                    System.out.println(orderItem.getProduct().getProductName() + " x" + orderItem.getOrderItemQuantity());
//                }
//            }
//        }


    }
    
}
