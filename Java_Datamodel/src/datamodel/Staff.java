/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.util.HashMap;

/**
 *
 * @author Alex
 */
public class Staff {
    @JsonProperty("STAFF_ID")
    private Integer staffId;
    @JsonProperty("STAFF_FIRST_NAME")
    private String firstName;
    @JsonProperty("STAFF_LAST_NAME")
    private String lastName;
    @JsonProperty("STAFF_EMAIL")
    private String email;
    @JsonProperty("STAFF_PHONE_NUMBER")
    private String phoneNumber;
    @JsonProperty("STAFF_DOB")
    private String dob;
    private LocalDate dateOfBirth;
    @JsonProperty("STAFF_POSITION")
    private String position;
    @JsonProperty("STAFF_SALARY")
    private Integer salary;
    @JsonProperty("STAFF_USERNAME")
    private String username;
    @JsonProperty("STAFF_PASSWORD")
    private String password;
    @JsonProperty("STORE_ID")
    private Integer storeId;
    @JsonProperty("STAFF_SEASONING")
    private String salt = "";
    
    public Staff() {
    }

    public Staff(Integer staffId, String firstName, String lastName, String email, String phoneNumber, LocalDate dateOfBirth, String position, Integer salary, String username, String password, Integer storeId) {
        this.staffId = staffId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.dateOfBirth = dateOfBirth;
        this.position = position;
        this.salary = salary;
        this.username = username;
        this.storeId = storeId;
        
        this.dob = this.dateOfBirth.toString();
        
        HashMap<String, String> hashedPassword = Hash.saltAndHashPassword(password);
        
        this.password = hashedPassword.get("password");
        this.salt = hashedPassword.get("salt");
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        HashMap<String, String> hashedPassword = Hash.saltAndHashPassword(password);
        
        this.password = hashedPassword.get("password");
        this.salt = hashedPassword.get("salt");
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
    
    public void convertDate(String dob) {
        this.setDateOfBirth(StringToLocalDate.convertToLocalDate(dob, "yyyy-MM-dd'T'HH:mm:ss"));
    }
}
