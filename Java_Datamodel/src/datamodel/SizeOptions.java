/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;

/**
 *
 * @author Alex
 */
public class SizeOptions {
    
    @JsonProperty("SIZE_ID")
    private Integer sizeId;
    @JsonProperty("SIZE_NAME")
    private String sizeName;
    @JsonProperty("PRODUCT_TYPE")
    private String productTypeString;
    
    private ProductType productType;

    public SizeOptions() {
    }

    public SizeOptions(Integer sizeId, String sizeName, ProductType productType) {
        this.sizeId = sizeId;
        this.sizeName = sizeName;
        this.productType = productType;
        
        convertProductTypeToString(productType);
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }
    
    public void convertProductTypeToString(ProductType productType) {
        this.productTypeString = productType.getProductType();
    }
    
    public void convertStringToProductType(String productTypeString) {
        for (ProductType productType : ModelController.getInstance().getProductTypeList()) {
            if (productType.getProductType().equals(productTypeString)) {
                this.productType = productType;
            }
        }
    }
}
