/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Alex
 */
public class DealItem {
    @JsonProperty("DEAL_ITEM_ID")
    private Integer dealItemId;
    @JsonProperty("DEAL_ID")
    private Integer dealId;
    @JsonProperty("PRODUCT_SIZE_ID")
    private Integer productSizeId;
    @JsonProperty("PRODUCT_TYPE")
    private String productType;
    @JsonProperty("QUANTITY")
    private Integer quantity;

    public DealItem() {
    }

    public DealItem(Integer dealItemId, String productType, Integer productSizeId, Integer quantity) {
        this.dealItemId = dealItemId;
        this.productType = productType;
        this.productSizeId = productSizeId;
        this.quantity = quantity;
    }

    public Integer getDealItemId() {
        return dealItemId;
    }

    public void setDealItemId(Integer dealItemId) {
        this.dealItemId = dealItemId;
    }

    public Integer getDealId() {
        return dealId;
    }

    public void setDealId(Integer dealId) {
        this.dealId = dealId;
    }

    public Integer getProductSizeId() {
        return productSizeId;
    }

    public void setProductSizeId(Integer productSizeId) {
        this.productSizeId = productSizeId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
