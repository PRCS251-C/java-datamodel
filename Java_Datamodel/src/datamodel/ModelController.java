/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import api.APIConnection;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 *
 * @author Alex
 */
public class ModelController {
    
    private static ModelController instance;
    
    private ArrayList<Customer> customerList = new ArrayList<>();
    private ArrayList<Deal> dealList = new ArrayList<>();
    private ArrayList<Ingredient> ingredientList = new ArrayList<>();    
    private ArrayList<Product> productList = new ArrayList<>();
    private ArrayList<ProductType> productTypeList = new ArrayList<>();
    private ArrayList<Order> orderList = new ArrayList<>();
    private ArrayList<SizeOptions> sizeOptionsList = new ArrayList<>();
    private ArrayList<Staff> staffList = new ArrayList<>();
    private ArrayList<Store> storeList = new ArrayList<>();

    protected ModelController() {
    }
    
    public static ModelController getInstance() {
        if (instance == null) {
            instance = new ModelController();
        }
        
        return instance;
    }
    
    // CUSTOMERS
    
    public void populateCustomerData() {
        ArrayList<Customer> tempCustomerList = APIConnection.getCustomerData();
        if (!tempCustomerList.isEmpty()) {
            this.setCustomerList(tempCustomerList);
        }
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }
    
    public Integer createCustomer(Integer customerId, String firstName, String lastName, String address, String postcode, String email, String phoneNumber, LocalDate dateOfBirth, String username, String password) {
        Customer customer = new Customer(customerId, firstName, lastName, address, postcode, email, phoneNumber, dateOfBirth, username, password);
        
        this.addCustomer(customer);
        
        Integer newCustomerId = APIConnection.postCustomerData(customer);
        
        this.customerList.get(this.customerList.size() - 1).setCustomerId(newCustomerId);
        
        return newCustomerId;
    }
    
    public Boolean addCustomer(Customer customer) {
        if (customer != null) {
            return this.customerList.add(customer);
        }
        
        return false;
    }
    
    public Boolean updateCustomer(Customer customer) {
        for (Customer currentCustomer : this.customerList) {
            if (currentCustomer.getCustomerId().equals(customer.getCustomerId())) {
                this.customerList.set(this.customerList.indexOf(currentCustomer), customer);
                break;
            }
        }
        
        return APIConnection.updateCustomerData(customer);
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.customerList = customerList;
    }
    
    public Boolean removeCustomer(Customer customer) {
        return this.customerList.remove(customer);
    }
    
    public Boolean customerLogin(String username, String password) {
        return APIConnection.customerLogin(username, password);
    }
    
    // DEALS
    
    public void populateDealData() {
        ArrayList<Deal> tempDealList = APIConnection.getDealData();
        if (!tempDealList.isEmpty()) {
            this.setDealList(tempDealList);
        }
    }
    
    public ArrayList<Deal> getDealList() {
        return dealList;
    }
    
    public void createDeal(Integer dealId, String dealName, String description, Integer price, Integer percentageDiscount, ArrayList<DealItem> dealItemsList) {
        Deal deal = new Deal(dealId, dealName, description, price, percentageDiscount, dealItemsList);
        
        this.addDeal(deal);
        
        Integer newDealId = APIConnection.postDealData(deal);
        
        this.dealList.get(this.dealList.size() - 1).setDealId(newDealId);
    }
    
    public Boolean addDeal(Deal deal) {
        if (deal != null) {
            return this.dealList.add(deal);
        }
        
        return false;
    }
    
    public Boolean updateDeal(Deal deal) {
        for (Deal currentDeal : this.dealList) {
            if (currentDeal.getDealId().equals(deal.getDealId())) {
                this.dealList.set(this.dealList.indexOf(currentDeal), deal);
                break;
            }
        }
        
        return APIConnection.updateDealData(deal);
    }

    public void setDealList(ArrayList<Deal> dealList) {
        this.dealList = dealList;
    }
    
    public Boolean removeDeal(Deal deal) {
        return this.dealList.remove(deal);
    }
    
    // INGREDIENTS
    
    public void populateIngredientData() {
        ArrayList<Ingredient> tempIngredientList = APIConnection.getIngredientData();
        if (!tempIngredientList.isEmpty()) {
            this.setIngredientList(tempIngredientList);
        }
    }
    
    public ArrayList<Ingredient> getIngredientList() {
        return ingredientList;
    }
    
    public void createIngredient(Integer ingredientId, String name, String description, Double additionalCost) {
        Ingredient ingredient = new Ingredient(ingredientId, name, description, additionalCost);
        
        this.addIngredient(ingredient);
        
        Integer newIngredientId = APIConnection.postIngredientData(ingredient);
        
        this.ingredientList.get(this.ingredientList.size() - 1).setIngredientId(newIngredientId);
    }
    
    public Boolean addIngredient(Ingredient ingredient) {
        if (ingredient != null) {
            return this.ingredientList.add(ingredient);
        }
        
        return false;
    }
    
    public Boolean updateIngredient(Ingredient ingredient) {
        for (Ingredient currentIngredient : this.ingredientList) {
            if (currentIngredient.getIngredientId().equals(ingredient.getIngredientId())) {
                this.ingredientList.set(this.ingredientList.indexOf(currentIngredient), ingredient);
                break;
            }
        }
        
        return APIConnection.updateIngredientData(ingredient);
    }

    public void setIngredientList(ArrayList<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }
    
    public Boolean removeIngredient(Ingredient ingredient) {
        return this.ingredientList.remove(ingredient);
    }
    
    // ORDERS
    
    public void populateOrderData() {
        ArrayList<Order> tempOrderList = APIConnection.getOrderData();
        System.out.println("Temp order list size: " + tempOrderList.size());
        if (!tempOrderList.isEmpty()) {
            this.setOrderList(tempOrderList);
            System.out.println("Order list size: " + this.orderList.size());
        }
    }
    
    public ArrayList<Order> getOrderList() {
        return orderList;
    }
    
    public Integer createOrder(Integer orderId, Double price, LocalDateTime orderTime, String status, Integer customerId, Integer paymentId, Integer staffId, ArrayList<OrderItem> orderItems, String deliveryAddress, String deliveryPostcode) {
        Order order = new Order(orderId, price, orderTime, status, customerId, paymentId, staffId, orderItems, deliveryAddress, deliveryPostcode);
       
        this.addOrder(order);
        
        Integer newOrderId = APIConnection.postOrderData(order);
        
        this.orderList.get(this.orderList.size() - 1).setOrderId(newOrderId);
        
        return newOrderId;
    }
    
    public Boolean addOrder(Order order) {
        if (order != null) {
            return this.orderList.add(order);
        }
        
        return false;
    }
    
    public Boolean updateOrder(Order order) {
        for (Order currentOrder : this.orderList) {
            if (currentOrder.getOrderId().equals(order.getOrderId())) {
                this.orderList.set(this.orderList.indexOf(currentOrder), order);
                break;
            }
        }
        
        return APIConnection.updateOrderData(order);
    }

    public void setOrderList(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }
    
    public Boolean removeOrder(Order order) {
        return this.orderList.remove(order);
    }
    
    // PRODUCTS
    
    public void populateProductData() {
        ArrayList<Product> tempProductList = APIConnection.getProductData();
        if (!tempProductList.isEmpty()) {
            this.setProductList(tempProductList);
        }
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }
    
    public void createProduct(Integer productId, String productName, String description, byte[] imageBlob, String productType, Boolean isProductCustomisable, ArrayList<Ingredient> ingredientList, ArrayList<ProductSizes> productSizes) {
        Product product = new Product(productId, productName, description, imageBlob, productType, isProductCustomisable, ingredientList, productSizes);
        
        this.addProduct(product);
        
        Integer newProductId = APIConnection.postProductData(product);
        
        this.productList.get(this.productList.size() - 1).setProductId(newProductId);
    }
    
    public Boolean addProduct(Product product) {
        if (product != null) {
            return this.productList.add(product);
        }
        
        return false;
    }
    
    public Boolean updateProduct(Product product) {
        for (Product currentProduct : this.productList) {
            if (currentProduct.getProductId().equals(product.getProductId())) {
                this.productList.set(this.productList.indexOf(currentProduct), product);
                break;
            }
        }
        
        return APIConnection.updateProductData(product);
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }
    
    public Boolean removeProduct(Product product) {
        return this.productList.remove(product);
    }
    
    // PRODUCT TYPES
    
    public void populateProductTypeData() {
        ArrayList<ProductType> tempProductTypeList = APIConnection.getProductTypeData();
        if (!tempProductTypeList.isEmpty()) {
            this.setProductTypeList(tempProductTypeList);
        }
    }
    
    public ArrayList<ProductType> getProductTypeList() {
        return productTypeList;
    }    
    
    public Boolean createProductType(String productTypeName) {
        ProductType productType = new ProductType(productTypeName);

        if (APIConnection.postProductTypeData(productType)) {
            return this.addProductType(productType);
        }
        
        return false;
    }
    
    public Boolean addProductType(ProductType productType) {
        if (productType != null) {
            return this.productTypeList.add(productType);
        }
        
        return false;
    }

    public Boolean updateProductType(ProductType productType) {
        for (ProductType currentProductType : this.productTypeList) {
            if (currentProductType.getProductType().equals(productType.getProductType())) {
                this.productTypeList.set(this.productTypeList.indexOf(currentProductType), productType);
            }
        }
        
        return APIConnection.updateProductTypeData(productType);
    }

    public void setProductTypeList(ArrayList<ProductType> productTypeList) {
        this.productTypeList = productTypeList;
    }
    
    public Boolean removeProductType(ProductType productType) {
        return this.productTypeList.remove(productType);
    }
    
    // SIZE OPTIONS
    
    public void populateSizeOptionsData() {
        ArrayList<SizeOptions> tempSizeOptionsList = APIConnection.getSizeOptionsData();
        if (!tempSizeOptionsList.isEmpty()) {
            this.sizeOptionsList = tempSizeOptionsList;
        }
    }

    public ArrayList<SizeOptions> getSizeOptionsList() {
        return sizeOptionsList;
    }
    
    public Boolean createSizeOptions(Integer sizeId, String sizeName, ProductType productType) {
        SizeOptions size = new SizeOptions(sizeId, sizeName, productType);

        if (APIConnection.postSizeOptionsData(size)) {
            return this.addSizeOptions(size);
        }
        
        return false;
    }
    
    public Boolean addSizeOptions(SizeOptions sizeOptions) {
        if (sizeOptions != null) {
            return this.sizeOptionsList.add(sizeOptions);
        }
        
        return false;
    }

    public Boolean updateSizeOptions(SizeOptions sizeOptions) {
        for (SizeOptions currentSizeOptions : this.sizeOptionsList) {
            if (currentSizeOptions.getSizeId().equals(sizeOptions.getSizeId())) {
                this.sizeOptionsList.set(this.productTypeList.indexOf(currentSizeOptions), sizeOptions);
            }
        }
        
        return APIConnection.updateSizeOptionsData(sizeOptions);
    }

    public void setSizeOptionsList(ArrayList<SizeOptions> sizeOptionsList) {
        this.sizeOptionsList = sizeOptionsList;
    }
    
    public Boolean removeSizeOption(SizeOptions sizeOptions) {
        return this.sizeOptionsList.remove(sizeOptions);
    }

    // STAFF
    
    public void populateStaffData() {
        ArrayList<Staff> tempStaffList = APIConnection.getStaffData();
        if (!tempStaffList.isEmpty()) {
            this.setStaffList(tempStaffList);
        }
    }

    public ArrayList<Staff> getStaffList() {
        return staffList;
    }
    
    public void createStaff(Integer staffId, String firstName, String lastName, String email, String phoneNumber, LocalDate dateOfBirth, String position, Integer salary, String username, String password, Integer storeId) {
        Staff staff = new Staff(staffId, firstName, lastName, email, phoneNumber, dateOfBirth, position, salary, username, password, storeId);
        
        this.addStaff(staff);
        
        Integer newStaffId = APIConnection.postStaffData(staff);
        
        this.staffList.get(this.staffList.size() - 1).setStaffId(newStaffId);
    }
    
    public Boolean addStaff(Staff staff) {
        if (staff != null) {
            return this.staffList.add(staff);
        }
        
        return false;
    }
    
    public Boolean updateStaff(Staff staff) {
        for (Staff currentStaff : this.staffList) {
            if (currentStaff.getStaffId().equals(staff.getStaffId())) {
                this.staffList.set(this.staffList.indexOf(currentStaff), staff);
                break;
            }
        }
        
        return APIConnection.updateStaffData(staff);
    }

    public void setStaffList(ArrayList<Staff> staffList) {
        this.staffList = staffList;
    }
    
    public Boolean removeStaff(Staff staff) {
        return this.staffList.remove(staff);
    }
    
    public Boolean staffLogin(String username, String password) {
        return APIConnection.staffLogin(username, password);
    }
    
    public Boolean managerLogin(String username, String password) {
        return APIConnection.managerLogin(username, password);
    }
    
    //STORES
    
    public void populateStoreData() {
        ArrayList<Store> tempStoreList = APIConnection.getStoreData();
        if (tempStoreList.isEmpty()) {
            this.setStoreList(tempStoreList);
        }
    }

    public ArrayList<Store> getStoreList() {
        return storeList;
    }
    
    public void createStore(Integer storeId, String address, String postcode, String phoneNumber) {
        Store store = new Store(storeId, address, postcode, phoneNumber);
        
        this.addStore(store);
        
        Integer newStoreId = APIConnection.postStoreData(store);
        
        this.storeList.get(this.storeList.size() - 1).setStoreId(newStoreId);
    }
    
    public Boolean addStore(Store store) {
        if (store != null) {
            return this.storeList.add(store);
        }
        
        return false;
    }
    
    public Boolean updateStore(Store store) {
        for (Store currentStore : this.storeList) {
            if (currentStore.getStoreId().equals(store.getStoreId())) {
                this.storeList.set(this.storeList.indexOf(currentStore), store);
                break;
            }
        }
        
        return APIConnection.updateStoreData(store);
    }

    public void setStoreList(ArrayList<Store> storeList) {
        this.storeList = storeList;
    }
    
    public Boolean removeStore(Store store) {
        return this.storeList.remove(store);
    }
}
