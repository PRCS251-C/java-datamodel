/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.HashMap;

/**
 *
 * @author Alex
 */
public class Customer {
    @JsonProperty("CUSTOMER_ID")
    private Integer customerId;
    @JsonProperty("CUSTOMER_FIRST_NAME")
    private String firstName;
    @JsonProperty("CUSTOMER_LAST_NAME")
    private String lastName;
    @JsonProperty("CUSTOMER_ADDRESS")
    private String address;
    @JsonProperty("CUSTOMER_POSTCODE")
    private String postcode;
    @JsonProperty("CUSTOMER_EMAIL")
    private String email;
    @JsonProperty("CUSTOMER_DOB")
    private String dob;
    private LocalDate dateOfBirth;
    @JsonProperty("CUSTOMER_USERNAME")
    private String username;
    @JsonProperty("CUSTOMER_PASSWORD")
    private String password;
    @JsonProperty("CUSTOMER_SEASONING")
    private String salt = "";
    @JsonProperty("CUSTOMER_PHONE_NUMB")
    private String phoneNumber;
    
    public Customer() {
    }

    public Customer(Integer customerId, String firstName, String lastName, String address, String postcode, String email, String phoneNumber, LocalDate dateOfBirth, String username, String password) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.postcode = postcode;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.dateOfBirth = dateOfBirth;
        this.username = username;
        
        this.dob = dateOfBirth.toString();
        
        HashMap<String, String> hashedPassword = Hash.saltAndHashPassword(password);
        
        this.password = hashedPassword.get("password");
        this.salt = hashedPassword.get("salt");
    }
    
    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
        
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        HashMap<String, String> hashedPassword = Hash.saltAndHashPassword(password);
        
        this.password = hashedPassword.get("password");
        this.salt = hashedPassword.get("salt");
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
    
    public void convertDate(String dateString) {
        this.setDateOfBirth(StringToLocalDate.convertToLocalDate(dateString, "yyyy-MM-dd'T'HH:mm:ss"));
    }
}
