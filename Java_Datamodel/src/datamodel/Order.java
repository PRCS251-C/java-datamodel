/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 *
 * @author Alex
 */
public class Order {
    @JsonProperty("ORDER_ID")
    private Integer orderId;
    @JsonProperty("ORDER_PRICE")
    private Double price;
    @JsonProperty("ORDER_TIME")
    private String orderTimeString;
    private LocalDateTime orderTime;
    @JsonProperty("ORDER_STATUS")
    private String status;
    @JsonProperty("CUSTOMER_ID")
    private Integer customerId;
    @JsonProperty("PAYMENT_ID")
    private Integer paymentId;
    @JsonProperty("STAFF_ID")
    private Integer staffId;
    @JsonProperty("DELIVERY_ADDRESS")
    private String deliveryAddress;
    @JsonProperty("DELIVERY_POSTCODE")
    private String deliveryPostcode;
    
    @JsonProperty("ORDER_ITEMS")
    private ArrayList<OrderItem> orderItems = new ArrayList<>();
    private Boolean isPrepared = false;
    private Payment payment = new Payment();
    private String displayString = "";

    public Order() {
    }

    public Order(Integer orderId, Double price, LocalDateTime orderTime, String status, Integer customerId, Integer paymentId, Integer staffId, ArrayList<OrderItem> orderItems, String deliveryAddress, String deliveryPostcode) {
        this.orderId = orderId;
        this.price = price;
        this.orderTime = orderTime;
        this.status = status;
        this.customerId = customerId;
        this.paymentId = paymentId;
        this.staffId = staffId;
        this.orderItems = orderItems;
        this.deliveryAddress = deliveryAddress;
        this.deliveryPostcode = deliveryPostcode;
        
        this.orderTimeString = this.orderTime.toString();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalDateTime getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(LocalDateTime orderTime) {
        this.orderTime = orderTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    public ArrayList<OrderItem> getOrderItems() {
        return orderItems;
    }
    
    public Boolean addOrderItems(OrderItem item) {
        if (item != null) {
            return this.orderItems.add(item);
        }
        
        return false;
    }
    
    public Boolean removeOrderItems(OrderItem item) {
        return this.orderItems.remove(item);
    }

    public void setOrderItems(ArrayList<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Boolean getIsPrepared() {
        return isPrepared;
    }

    public void setIsPrepared(Boolean isPrepared) {
        this.isPrepared = isPrepared;
    }

    public String getOrderTimeString() {
        return orderTimeString;
    }

    public void setOrderTimeString(String orderTimeString) {
        this.orderTimeString = orderTimeString;
    }
    
    public void convertDateTime(String dateTimeString) {
        this.setOrderTime(StringToLocalDate.convertToLocalDateTime(dateTimeString, "yyyy-MM-dd'T'HH:mm:ss"));
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getDisplayString() {
        return displayString;
    }

    public void setDisplayString(String displayString) {
        this.displayString = displayString;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryPostcode() {
        return deliveryPostcode;
    }

    public void setDeliveryPostcode(String deliveryPostcode) {
        this.deliveryPostcode = deliveryPostcode;
    }
    
    @Override
    public String toString() {
        return displayString;
    }
}
