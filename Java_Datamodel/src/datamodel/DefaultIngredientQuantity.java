/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Alex
 */
public class DefaultIngredientQuantity {
    @JsonProperty("INGREDIENT_ID")
    private Integer ingredientId;
    @JsonProperty("PRODUCT_ID")
    private Integer productId;
    @JsonProperty("QUANTITY")
    private Integer quantity;

    public DefaultIngredientQuantity() {
    }

    public DefaultIngredientQuantity(Integer ingredientId, Integer productId, Integer quantity) {
        this.ingredientId = ingredientId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public Integer getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    
    
}
