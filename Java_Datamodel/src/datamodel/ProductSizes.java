/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Alex
 */
public class ProductSizes {
    
    @JsonProperty("PRODUCT_ID")
    private Integer productId;
    @JsonProperty("SIZE_ID")
    private Integer sizeId;
    @JsonProperty("PRICE")
    private Double price;
    
    private String sizeName;

    public ProductSizes() {
    }

    public ProductSizes(Integer productId, Integer sizeId, Double price) {
        this.productId = productId;
        this.sizeId = sizeId;
        this.price = price;
    }
    
    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }
}
