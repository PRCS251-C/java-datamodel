/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Alex
 */
public class Ingredient {
    @JsonProperty("INGREDIENT_ID")
    private Integer ingredientId;
    @JsonProperty("INGREDIENT_NAME")
    private String name;
    @JsonProperty("INGREDIENT_DESCRIPTION")
    private String description;
    @JsonProperty("INGREDIENT_ADDITIONAL_COST")
    private Double additionalCost;
    
    private Integer quantity;

    public Ingredient() {
    }

    public Ingredient(Integer ingredientId, String name, String description, Double additionalCost) {
        this.ingredientId = ingredientId;
        this.name = name;
        this.description = description;
        this.additionalCost = additionalCost;
    }

    public Integer getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAdditionalCost() {
        return additionalCost;
    }

    public void setAdditionalCost(Double additionalCost) {
        this.additionalCost = additionalCost;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    
    
}
