/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;

/**
 *
 * @author Alex
 */
public class Product {
    @JsonProperty("PRODUCT_ID")
    private Integer productId;
    @JsonProperty("PRODUCT_NAME")
    private String productName;
    @JsonProperty("PRODUCT_DESCRIPTION")
    private String description;
    @JsonProperty("PRODUCT_IMAGE")
    private String imageBlobString;
    private byte[] imageBlob;
    @JsonProperty("PRODUCT_TYPE")
    private String productType;
    @JsonProperty("PRODUCT_IS_CUSTOMISABLE")
    private String isProductCustomisableChar;
    
    private Boolean isProductCustomisable;
    
    @JsonProperty("PRODUCT_SIZES")
    private ArrayList<ProductSizes> productSizes = new ArrayList<>();
    @JsonProperty("INGREDIENT_LIST")
    private ArrayList<Ingredient> ingredientList = new ArrayList<>();
    
    public Product() {
    }

    public Product(Integer productId, String productName, String description, byte[] imageBlob, String productType, Boolean isProductCustomisable, ArrayList<Ingredient> ingredientList, ArrayList<ProductSizes> productSizes) {
        this.productId = productId;
        this.productName = productName;
        this.description = description;
        this.imageBlob = imageBlob;
        this.productType = productType;
        this.isProductCustomisable = isProductCustomisable;
        this.ingredientList = ingredientList;
        this.productSizes = productSizes;
        
        convertCustomisableBoolToChar(this.isProductCustomisable);
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImageBlob() {
        return imageBlob;
    }

    public void setImageBlob(byte[] imageBlob) {
        this.imageBlob = imageBlob;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getIsProductCustomisableChar() {
        return isProductCustomisableChar;
    }

    public void setIsProductCustomisableChar(String isProductCustomisableChar) {
        this.isProductCustomisableChar = isProductCustomisableChar;
    }

    public Boolean getIsProductCustomisable() {
        return isProductCustomisable;
    }

    public void setIsProductCustomisable(Boolean isProductCustomisable) {
        this.isProductCustomisable = isProductCustomisable;
    }

    public ArrayList<ProductSizes> getProductSizes() {
        return productSizes;
    }

    public void setProductSizes(ArrayList<ProductSizes> productSizes) {
        this.productSizes = productSizes;
    }
    
    public Boolean addProductSize(ProductSizes productSizes) {
        if (productSizes != null) {
            return this.productSizes.add(productSizes);
        }
        
        return false;
    }
    
    public Boolean removeProductSize(ProductSizes productSizes) {
        return this.productSizes.remove(productSizes);
    }

    public ArrayList<Ingredient> getIngredientList() {
        return ingredientList;
    }
    
    public Boolean addIngredient(Ingredient ingredient) {
        if (ingredient != null) {
            return this.ingredientList.add(ingredient);
        }
        
        return false;
    }
    
    public Boolean removeIngredient(Ingredient ingredient) {
        return this.ingredientList.remove(ingredient);
    }

    public void setIngredientList(ArrayList<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public String getImageBlobString() {
        return imageBlobString;
    }

    public void setImageBlobString(String imageBlobString) {
        this.imageBlobString = imageBlobString;
    }
    
    public void convertCustomisableBoolToChar(Boolean isCustomisable) {
        if (isCustomisable) {
            this.isProductCustomisableChar = "1";
        } else {
            this.isProductCustomisableChar = "0";
        }
    }
    
    public void convertCustomisableCharToBool(String isCustomisable) {
        if (isCustomisable.equals("1")) {
            this.isProductCustomisable = true;
        } else {
            this.isProductCustomisable = false;
        }
    }
}
