/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;

/**
 *
 * @author Alex
 */
public class Payment {
    @JsonProperty("PAYMENT_ID")
    private Integer paymentId;
    @JsonProperty("PAYMENT_METHOD")
    private String paymentMethod;
    @JsonProperty("PAYMENT_EMAIL")
    private String paypalEmail;
    @JsonProperty("CARD_TYPE")
    private String cardType;
    @JsonProperty("CARD_NUMBER")
    private String cardNumber;
    @JsonProperty("CARD_NAME")
    private String cardName;
    @JsonProperty("CARD_ISSUE_DATE")
    private String cardIssueDate;
    private LocalDate issueDate;
    @JsonProperty("CARD_EXPIRY_DATE")
    private String cardExpiryDate;
    private LocalDate expiryDate;
    @JsonProperty("CARD_CVC")
    private Integer cardCvc;
    @JsonProperty("CARD_ISSUE_NUMBER")
    private Integer cardIssueNumber;
    @JsonProperty("BILLING_ADDRESS")
    private String billingAddress;
    @JsonProperty("BILLING_POSTCODE")
    private String billingPostcode;

    public Payment() {
    }

    public Payment(Integer paymentId, String paymentMethod, String paypalEmail, String cardType, String cardNumber, String cardName, LocalDate issueDate, LocalDate expiryDate, Integer cardCvc, Integer cardIssueNumber, String billingAddress, String billingPostcode) {
        this.paymentId = paymentId;
        this.paymentMethod = paymentMethod;
        this.paypalEmail = paypalEmail;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.cardName = cardName;
        this.issueDate = issueDate;
        this.expiryDate = expiryDate;
        this.cardCvc = cardCvc;
        this.cardIssueNumber = cardIssueNumber;
        this.billingAddress = billingAddress;
        this.billingPostcode = billingPostcode;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaypalEmail() {
        return paypalEmail;
    }

    public void setPaypalEmail(String paypalEmail) {
        this.paypalEmail = paypalEmail;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardIssueDate() {
        return cardIssueDate;
    }

    public void setCardIssueDate(String cardIssueDate) {
        this.cardIssueDate = cardIssueDate;
    }

    public String getCardExpiryDate() {
        return cardExpiryDate;
    }

    public void setCardExpiryDate(String cardExpiryDate) {
        this.cardExpiryDate = cardExpiryDate;
    }

    public Integer getCardCvc() {
        return cardCvc;
    }

    public void setCardCvc(Integer cardCvc) {
        this.cardCvc = cardCvc;
    }

    public Integer getCardIssueNumber() {
        return cardIssueNumber;
    }

    public void setCardIssueNumber(Integer cardIssueNumber) {
        this.cardIssueNumber = cardIssueNumber;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingPostcode() {
        return billingPostcode;
    }

    public void setBillingPostcode(String billingPostcode) {
        this.billingPostcode = billingPostcode;
    } 

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public void convertIssueDate(String dateString) {
        this.setIssueDate(StringToLocalDate.convertToLocalDate(dateString, "yyyy-MM-dd'T'HH:mm:ss"));
    }
    
    public void convertExpiryDate(String dateString) {
        this.setExpiryDate(StringToLocalDate.convertToLocalDate(dateString, "yyyy-MM-dd'T'HH:mm:ss"));
    }
}
