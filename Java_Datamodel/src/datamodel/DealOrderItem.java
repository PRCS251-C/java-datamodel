/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Alex
 */
public class DealOrderItem {
    
    @JsonProperty("DEAL_ORDER_ITEM_ID")
    private Integer dealOrderItemId;
    @JsonProperty("DEAL_ID")
    private Integer dealId;
    @JsonProperty("ORDER_ITEM_ID")
    private Integer orderItemId;

    public DealOrderItem() {
    }

    public DealOrderItem(Integer dealOrderItemId, Integer dealId, Integer orderItemId) {
        this.dealOrderItemId = dealOrderItemId;
        this.dealId = dealId;
        this.orderItemId = orderItemId;
    }
    
    public Integer getDealOrderItemId() {
        return dealOrderItemId;
    }

    public void setDealOrderItemId(Integer dealOrderItemId) {
        this.dealOrderItemId = dealOrderItemId;
    }

    public Integer getDealId() {
        return dealId;
    }

    public void setDealId(Integer dealId) {
        this.dealId = dealId;
    }

    public Integer getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Integer orderItemId) {
        this.orderItemId = orderItemId;
    }
}
