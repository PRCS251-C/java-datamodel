/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Alex
 */
public class Store {
    @JsonProperty("STORE_ID")
    private Integer storeId;
    @JsonProperty("STORE_ADDRESS")
    private String address;
    @JsonProperty("STORE_POSTCODE")
    private String postcode;
    @JsonProperty("STORE_PHONE_NUMBER")
    private String phoneNumber;

    public Store() {
    }

    public Store(Integer storeId, String address, String postcode, String phoneNumber) {
        this.storeId = storeId;
        this.address = address;
        this.postcode = postcode;
        this.phoneNumber = phoneNumber;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    
}
