/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;

/**
 *
 * @author Alex
 */
public class Deal {
    @JsonProperty("DEAL_ID")
    private Integer dealId;
    @JsonProperty("DEAL_NAME")
    private String dealName;
    @JsonProperty("DEAL_DESCRIPTION")
    private String description;
    @JsonProperty("DEAL_PRICE")
    private Integer price;
    @JsonProperty("DEAL_PERCENTAGE_DISCOUNT")
    private Integer percentageDiscount;
    
    @JsonProperty("DEAL_ITEMS")
    private ArrayList<DealItem> dealItemsList = new ArrayList<>();

    public Deal() {
    }

    public Deal(Integer dealId, String dealName, String description, Integer price, Integer percentageDiscount, ArrayList<DealItem> dealItemsList) {
        this.dealId = dealId;
        this.dealName = dealName;
        this.description = description;
        this.price = price;
        this.percentageDiscount = percentageDiscount;
        this.dealItemsList = dealItemsList;
        
        for (int i = 0; i < this.dealItemsList.size(); i++) {
            this.dealItemsList.get(i).setDealId(this.dealId);
        }
    }

    public Integer getDealId() {
        return dealId;
    }

    public void setDealId(Integer dealId) {
        this.dealId = dealId;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPercentageDiscount() {
        return percentageDiscount;
    }

    public void setPercentageDiscount(Integer percentageDiscount) {
        this.percentageDiscount = percentageDiscount;
    }

    public ArrayList<DealItem> getDealItemsList() {
        return dealItemsList;
    }
    
    public Boolean addDealItem(DealItem dealItem) {
        if (dealItem != null) {
            return this.dealItemsList.add(dealItem);
        }
        
        return false;
    }

    public void setDealItemsList(ArrayList<DealItem> dealItemsList) {
        this.dealItemsList = dealItemsList;
    }
    
    public Boolean removeDealItem(DealItem dealItem) {
        return this.dealItemsList.remove(dealItem);
    }
}
