/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;

/**
 *
 * @author Alex
 */
public class OrderItem {
    @JsonProperty("ORDER_ITEM_ID")
    private Integer orderItemId;
    @JsonProperty("ORDER_ITEM_QUANTITY")
    private Integer orderItemQuantity;
    @JsonProperty("ORDER_ITEM_SIZE")
    private Integer orderItemSizeId;
    @JsonProperty("ORDER_ID")
    private Integer orderId;
    @JsonProperty("PRODUCT_ID")
    private Integer productId;
    
    private Product product;
    private String orderItemSize;
    private Boolean isPrepared = false;
    private ArrayList<String> customisations = new ArrayList<>();
    
    @JsonProperty("CUSTOM_INGREDIENTS")
    private ArrayList<Ingredient> customIngredients = new ArrayList<>();

    public OrderItem() {
    }

    public OrderItem(Integer orderItemId, Integer orderItemQuantity, String orderItemSize, Integer orderId, Integer productId, ArrayList<Ingredient> customIngredients) {
        this.orderItemId = orderItemId;
        this.orderItemQuantity = orderItemQuantity;
        this.orderItemSize = orderItemSize;
        this.orderId = orderId;
        this.productId = productId;
        
        if (customIngredients != null) {
            this.customIngredients = customIngredients;
        }
    }

    public Integer getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Integer orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getOrderItemQuantity() {
        return orderItemQuantity;
    }

    public void setOrderItemQuantity(Integer orderItemQuantity) {
        this.orderItemQuantity = orderItemQuantity;
    }

    public String getOrderItemSize() {
        return orderItemSize;
    }

    public void setOrderItemSize(String orderItemSize) {
        this.orderItemSize = orderItemSize;
    }

    // TODO: FIX THIS
    public Integer getOrderItemSizeId() {
        return orderItemSizeId;
    }

    // TODO: FIX THIS
    public void setOrderItemSizeId(Integer orderItemSizeId) {
        this.orderItemSizeId = orderItemSizeId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Boolean getIsPrepared() {
        return isPrepared;
    }

    public void setIsPrepared(Boolean isPrepared) {
        this.isPrepared = isPrepared;
    }

    public ArrayList<String> getCustomisations() {
        return customisations;
    }
    
    public Boolean addCustomisation(String customisation) {
        if (customisation != null) {
            return this.customisations.add(customisation);
        }
        
        return false;
    }

    public void setCustomisations(ArrayList<String> customisations) {
        this.customisations = customisations;
    }
    
    public Boolean removeCustomisation(String customisation) {
        return this.customisations.remove(customisation);
    }
}
