/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Alex
 */
public class OrderItemIngredientQuantity {
    @JsonProperty("ORDER_ITEM_ID")
    private Integer orderItemId;
    @JsonProperty("INGREDIENT_ID")
    private Integer ingredientId;
    @JsonProperty("QUANTITY")
    private Integer quantity;

    public OrderItemIngredientQuantity() {
    }

    public OrderItemIngredientQuantity(Integer orderItemId, Integer ingredientId, Integer quantity) {
        this.orderItemId = orderItemId;
        this.ingredientId = ingredientId;
        this.quantity = quantity;
    }

    public Integer getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Integer orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    
    
}
