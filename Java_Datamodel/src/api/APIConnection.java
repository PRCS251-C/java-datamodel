/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import datamodel.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import sun.misc.IOUtils;

/**
 *
 * @author Alex
 */
public class APIConnection {
    
    public static ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT);
    
    protected APIConnection(){
    }
    
    // API FUNCTION CALLS
    
//    public static byte[] getData(String endpoint) {
//        try {
//            String uri = "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/" + endpoint;
//            URL url = new URL(uri);
//            
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setRequestMethod("GET");
//            connection.setRequestProperty("Content-Type", "application/json");
//            connection.setRequestProperty("Accept", "application/json");
//            
//            InputStream input = connection.getInputStream();
//            
//            ByteArrayOutputStream result = new ByteArrayOutputStream();
//            byte[] buffer = new byte[1024];
//            int length;
//            while ((length = input.read(buffer)) != -1) {
//                result.write(buffer, 0, length);
//            }
//            return buffer;
//        } catch (Exception ex) {
//            
//        }
//        return null;
//    }
    
    public static Object[] getData(String tableName, Class tableClass) {
        Object[] object = null;
        
        try {
            String uri = "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/" + tableName;
            URL url = new URL(uri);
            
            object = (Object[]) mapper.readValue(url, tableClass);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return object;
    }
    
    public static Object getData(String tableName, Class tableClass, Integer id) {
        Object object = new Object();
        
        try {
            String uri = "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/" + tableName + "/" + id;
            URL url = new URL(uri);
            
            object = mapper.readValue(url, tableClass);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return object;
    }
    
    public static Integer putData(String endpoint, Object obj) {
        try {
            String uri = "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/" + endpoint;
            URL url = new URL(uri);
            
            String json = mapper.writeValueAsString(obj);
            
            System.out.println(json);
            
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("PUT");
            connection.setDoOutput(true);
            
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            
            try (OutputStreamWriter output = new OutputStreamWriter(connection.getOutputStream())) {
                output.write(json);
                output.flush();
            }
            
            return connection.getResponseCode();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return 400;
    }
    
    public static HashMap<String, Object> postData(String endpoint, Object obj) {
        HashMap<String, Object> response = new HashMap<>();
        
        try {
            String uri = "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/" + endpoint;
            URL url = new URL(uri);
            
            String json = mapper.writeValueAsString(obj);
            
            System.out.println(json);
            
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            //connection.setRequestProperty("Authorization", /* AUTH CONTENT */ "");
            
            try (OutputStreamWriter output = new OutputStreamWriter(connection.getOutputStream())) {
                output.write(json);
                output.flush();
            }
            
            System.out.println(connection.getResponseMessage());
            
            response.put("responseCode", connection.getResponseCode());
            
            if (connection.getResponseCode() >= 200 && connection.getResponseCode() < 300) {
                response.put("responseMessage", new BufferedReader(new InputStreamReader((connection.getInputStream()))).readLine());
            } else {
                response.put("responseMessage", null);
            }
            
            return response;
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        response.put("responseCode", 400);
        response.put("responseMessage", null);
        
        return response;
    }
    
    public static Integer deleteData(String endpoint) {
        try {
            String uri = "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/" + endpoint;
            URL url = new URL(uri);
            
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("DELETE");
            
            System.out.println(connection.getResponseCode() + " " + connection.getResponseMessage());
            
            return connection.getResponseCode();
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return 400;
    }
    
    // LOGIN REQUESTS
    
    public static Boolean customerLogin(String username, String password) {
        HashMap<String, String> details = new HashMap<>();
        
        String salt = getCustomerSalt(username);
        password = Hash.hashPassword(password + salt);
        
        details.put("password", password);
        details.put("username", username);
        
        HashMap<String, Object> response = postData("Customer/Login", details);
        
        Integer responseCode = (Integer) response.get("responseCode");
        
        System.out.println(responseCode);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean staffLogin(String username, String password) {
        HashMap<String, String> details = new HashMap<>();
        
        String salt = getStaffSalt(username);
        password = Hash.hashPassword(password + salt);
        
        details.put("password", password);
        details.put("username", username);
        
        HashMap<String, Object> response = postData("Staff/Login", details);
        
        Integer responseCode = (Integer) response.get("responseCode");
        
        System.out.println(responseCode);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean managerLogin(String username, String password) {
        if (staffLogin(username, password)) {
            String staffPosition = getStaffPosition(username);
            
            if (staffPosition.equals("Manager")) {
                return true;
            }
        }
        
        return false;
    }
    
    // GET REQUESTS
    
    public static ArrayList<Customer> getCustomerData() {
        Customer[] customerArray = (Customer[]) getData("Customer", Customer[].class);
        ArrayList<Customer> customerList = new ArrayList<>();
        
        for (Customer currentCustomer : customerArray) {
            currentCustomer.convertDate(currentCustomer.getDob()); //Unpleasent fix for JSON parsing issue
            customerList.add(currentCustomer);
        }
        
        return customerList;
    }
    
    public static Customer getCustomerData(Integer id) {
        Customer customer = (Customer) getData("Customer", Customer.class, id);
        
        customer.convertDate(customer.getDob());
        
        return customer;
    }
    
    public static String getCustomerSalt(String username) {
        return (String) getData("Customer/" + username + "/Salt", String[].class)[0];
    }
    
    public static Customer getCustomerByUsername(String username) {
        Customer[] customer = (Customer[]) getData("Customer/Username/" + username, Customer[].class);
        
        customer[0].convertDate(customer[0].getDob());
        
        return customer[0];
    }
    
    public static ArrayList<Deal> getDealData() {
        Deal[] dealArray;
        ArrayList<Deal> dealList = new ArrayList<>();

        dealArray = (Deal[]) getData("Deal", Deal[].class);
            
        dealList.addAll(Arrays.asList(dealArray));
        
        for (Deal deal : dealList) {
            deal.setDealItemsList(getDealItemForDealData(deal.getDealId()));
        }

        return dealList;
    }
    
    public static Deal getDealData(Integer id) {
        return (Deal) getData("Deal", Deal.class, id);
    }
    
    public static ArrayList<DealItem> getDealItemData() {
        DealItem[] dealItemArray;
        ArrayList<DealItem> dealItemList = new ArrayList<>();

        dealItemArray = (DealItem[]) getData("DealItem", DealItem[].class);
            
        dealItemList.addAll(Arrays.asList(dealItemArray));

        return dealItemList;
    }
    
    public static DealItem getDealItemData(Integer id) {
        return (DealItem) getData("DealItem", DealItem.class, id);
    }
    
    public static ArrayList<DealItem> getDealItemForDealData(Integer id) {
        DealItem[] dealItemArray;
        ArrayList<DealItem> dealItemList = new ArrayList<>();

        dealItemArray = (DealItem[]) getData("Deal/" + id + "/Items", DealItem[].class);
            
        dealItemList.addAll(Arrays.asList(dealItemArray));

        return dealItemList;
    }

    public static ArrayList<DealOrderItem> getDealOrderItemData() {
        DealOrderItem[] dealOrderItemArray;
        ArrayList<DealOrderItem> dealOrderItemList = new ArrayList<>();

        dealOrderItemArray = (DealOrderItem[]) getData("DealOrderItem", DealOrderItem[].class);
            
        dealOrderItemList.addAll(Arrays.asList(dealOrderItemArray));

        return dealOrderItemList;
    }
    
    public static DealOrderItem getDealOrderItemData(Integer id) {
        return (DealOrderItem) getData("DealOrderItem", DealOrderItem.class, id);
    }
    
    public static ArrayList<Ingredient> getIngredientData() {
        Ingredient[] ingredientArray;
        ArrayList<Ingredient> ingredientList = new ArrayList<>();

        ingredientArray = (Ingredient[]) getData("Ingredient", Ingredient[].class);
            
        ingredientList.addAll(Arrays.asList(ingredientArray));

        return ingredientList;
    }
    
    public static Ingredient getIngredientData(Integer id) {
        return (Ingredient) getData("Ingredient", Ingredient.class, id);
    }
    
    public static ArrayList<Order> getOrderData() {
        Order[] orderArray;
        ArrayList<Order> orderList = new ArrayList<>();

        //Get all orders from database (API filters out all that are fulfilled)
        orderArray = (Order[]) getData("Order", Order[].class);

        //Loop through orders to add order items and the customisations to those order items
        for (int i = 0; i < orderArray.length; i++) {

            orderArray[i] = handleOrderItemDataForOrder(orderArray[i]);
            orderList.add(orderArray[i]);
            
        }

        return orderList;
    }
    
    public static Order getOrderData(Integer id) {
        Order order = (Order) getData("Order", Order.class, id);
        
        order = handleOrderItemDataForOrder(order);
        
        return order;
    }
    
    /**
     * CAUTION - HERE BE DRAGONS!
     * WARNING - MAY CAUSE MASSIVE LAG ISSUES - 3 DEEP NESTED FOR LOOPS
     * Nests so large a pterodactyl could raise a child
     * @return Order object containing all order items, ingredients in the order items and customisations within those order items
     */
    public static Order handleOrderItemDataForOrder(Order order) {
        
        //Here's where stuff gets complicated!
        try {
            //Convert the order time string to a LocalDateTime if it is present
            if ((order.getOrderTimeString() != null) && (!order.getOrderTimeString().equals(""))) {
                order.convertDateTime(order.getOrderTimeString());
            }

            //Get and set all order items for the particular order id
            order.setOrderItems(getOrderItemDataForOrder(order.getOrderId()));
            
            //Get and set order payment information
            order.setPayment(getPaymentData(order.getPaymentId()));

            //Loop over the order items for this particular order to add the customisations (if there are any)
            for (int i = 0; i < order.getOrderItems().size(); i++) {

                //Add product to order item
                if (order.getOrderItems().get(i).getProductId() != null) {
                    order.getOrderItems().get(i).setProduct(getProductData(order.getOrderItems().get(i).getProductId()));
                }

                //Add product size info to order item
                if (order.getOrderItems().get(i).getOrderItemSizeId() != null) {
                    order.getOrderItems().get(i).setOrderItemSize(getSizeNameData(order.getOrderItems().get(i).getOrderItemSizeId()));
                }

                ArrayList<String> customisations = new ArrayList<>();

                //Gets all the ingredients for the current order item (obtained through top tier for loop)
                ArrayList<Ingredient> customIngredientList = getOrderItemIngredientData(order.getOrderItems().get(i).getOrderItemId());

                //Check if the order item atually has any ingredients
                if (!customIngredientList.isEmpty()) {
                    
                    //Get all the ingredients for the product this order item refers to
                    ArrayList<Ingredient> defaultIngredientList = getProductIngredientData(order.getOrderItems().get(i).getProductId());
                    ArrayList<Ingredient> largestList;
                    ArrayList<Ingredient> smallestList;
                    
                    String largest;

                    //Make sure we itterate over the largest of the two arrays
                    if (defaultIngredientList.size() > customIngredientList.size()) {
                        
                        largestList = defaultIngredientList;
                        smallestList = customIngredientList;
                        largest = "default";
                        
                    } else {
                        
                        largestList = customIngredientList;
                        smallestList = defaultIngredientList;
                        largest = "custom";
                        
                    }

                    //Loop over the largest array to compare the custom ingredients (if present) to the default ingredients
                    //to get the customisations
                    for (int j = 0; j < largestList.size(); j++) {
                        
                        Boolean ingredientFound = false;
                        
                        for (int k = 0; k < smallestList.size(); k++) {
                            
                            if ((largestList.get(j).getIngredientId().equals(smallestList.get(k).getIngredientId()))
                                 && !(largestList.get(j).getQuantity().equals(smallestList.get(k).getQuantity()))) {
                                //Same ingredient different quantity
                                
                                ingredientFound = true;
                                
                                customisations.add("" + customIngredientList.get(j).getName() + " x" + customIngredientList.get(j).getQuantity());
                                
                                break;

                            } else if (largestList.get(j).getIngredientId().equals(smallestList.get(k).getIngredientId())) {

                                //Ingredient is in both lists
                                
                                ingredientFound = true;

                                break;
                                
                            }
                        }
                        
                        if (!ingredientFound) {
                            //Ingredient is only in one list
                            
                            if (largest.equals("default")) {
                                customisations.add("No " + customIngredientList.get(j).getName());
                            } else {
                                customisations.add("Add " + customIngredientList.get(j).getName() + " x" + customIngredientList.get(j).getQuantity());
                            }
                        }
                    }
                }

                //Set the customisations to the order item
                order.getOrderItems().get(i).setCustomisations(customisations);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        return order;
    }
    
    public static ArrayList<OrderItem> getOrderItemData() {
        OrderItem[] orderItemArray;
        ArrayList<OrderItem> orderItemList = new ArrayList<>();
        
        orderItemArray = (OrderItem[]) getData("OrderItem", OrderItem[].class);
        
        orderItemList.addAll(Arrays.asList(orderItemArray));
        
        return orderItemList;
    }
    
    public static OrderItem getOrderItemData(Integer id) {
        return (OrderItem) getData("OrderItem", OrderItem.class, id);
    }
    
    public static ArrayList<OrderItem> getOrderItemDataForOrder(Integer id) {
        OrderItem[] orderItemArray;
        ArrayList<OrderItem> orderItemList = new ArrayList<>();
        
        orderItemArray = (OrderItem[]) getData("Order/" + id + "/Items", OrderItem[].class);
        
        if (orderItemArray != null) {
            orderItemList.addAll(Arrays.asList(orderItemArray));
        }
        
        return orderItemList;
    }
    
    public static ArrayList<Ingredient> getOrderItemIngredientData(Integer id) {
        OrderItemIngredientQuantity[] orderItemIngredientArray;
        ArrayList<Ingredient> ingredientList = new ArrayList<>();

        orderItemIngredientArray = (OrderItemIngredientQuantity[]) getData("OrderItem/" + id + "/Ingredients/Custom", OrderItemIngredientQuantity[].class);
            
        if (orderItemIngredientArray != null) {
            for (int i = 0; i < orderItemIngredientArray.length; i++) {
                ingredientList.add(getIngredientData(orderItemIngredientArray[i].getIngredientId()));
                ingredientList.get(i).setQuantity(orderItemIngredientArray[i].getQuantity());
            }
        }

        return ingredientList;
    }
    
    public static ArrayList<Payment> getPaymentData() {
        Payment[] paymentArray;
        ArrayList<Payment> paymentList = new ArrayList<>();
        
        paymentArray = (Payment[]) getData("Payment", Payment[].class);
        
        for (Payment currentPayment : paymentArray) {
            currentPayment.convertExpiryDate(currentPayment.getCardExpiryDate());
            currentPayment.convertIssueDate(currentPayment.getCardExpiryDate());
            paymentList.add(currentPayment);
        }
        
        return paymentList;
    }
    
    public static Payment getPaymentData(Integer id) {
        if (id != null) {
            Payment payment = (Payment) getData("Payment", Payment.class, id);

            payment.convertExpiryDate(payment.getCardExpiryDate());
            payment.convertIssueDate(payment.getCardIssueDate());

            return payment;
        }
        
        return null;
    }
    
    public static ArrayList<Product> getProductData() {
        Product[] productArray;
        ArrayList<Product> productList = new ArrayList<>();

        productArray = (Product[]) getData("Product", Product[].class);
        
        for (Product currentProduct : productArray) {
            currentProduct.setProductSizes(getProductSizesDataForProduct(currentProduct.getProductId()));
            for (ProductSizes size : currentProduct.getProductSizes()) {
                size.setSizeName(getSizeNameData(size.getSizeId()));
            }
            currentProduct.convertCustomisableCharToBool(currentProduct.getIsProductCustomisableChar());
            currentProduct.setIngredientList(getProductIngredientData(currentProduct.getProductId()));
        }

        productList.addAll(Arrays.asList(productArray));

        return productList;
    }    
    
    public static Product getProductData(Integer id) {
        Product product = (Product) getData("Product", Product.class, id);
        
        product.setProductSizes(getProductSizesDataForProduct(product.getProductId()));
        product.convertCustomisableCharToBool(product.getIsProductCustomisableChar());
        product.setIngredientList(getProductIngredientData(product.getProductId()));
        
        return product;
    }
    
    public static ArrayList<ProductType> getProductTypeData() {
        ProductType[] productArray;
        ArrayList<ProductType> productTypeList = new ArrayList<>();

        productArray = (ProductType[]) getData("ProductType", ProductType[].class);

        productTypeList.addAll(Arrays.asList(productArray));

        return productTypeList;
    }
    
    public static ArrayList<ProductSizes> getProductSizesData() {
        ProductSizes[] productSizesArray;
        ArrayList<ProductSizes> productSizesList = new ArrayList<>();

        productSizesArray = (ProductSizes[]) getData("ProductSizes", ProductSizes[].class);

        productSizesList.addAll(Arrays.asList(productSizesArray));

        return productSizesList;
    }    
    
    public static ProductSizes getProductSizesData(Integer id) {
        return (ProductSizes) getData("ProductSizes", ProductSizes.class, id);
    }
    
    public static ArrayList<ProductSizes> getProductSizesDataForProduct(Integer id) {
        ProductSizes[] productSizesArray;
        ArrayList<ProductSizes> productSizesList = new ArrayList<>();

        productSizesArray = (ProductSizes[]) getData("Product/" + id + "/Sizes", ProductSizes[].class);

        productSizesList.addAll(Arrays.asList(productSizesArray));

        return productSizesList;
    }
    
    public static ArrayList<SizeOptions> getSizeOptionsData() {
        SizeOptions[] sizeOptionsArray;
        ArrayList<SizeOptions> sizeOptionsList = new ArrayList<>();
        
        sizeOptionsArray = (SizeOptions[]) getData("SizeOptions", SizeOptions[].class);
        
        sizeOptionsList.addAll(Arrays.asList(sizeOptionsArray));
        
        return sizeOptionsList;
    }
    
    public static String getSizeNameData(Integer id) {
        return ((SizeOptions) getData("SizeOptions", SizeOptions.class, id)).getSizeName();
    }
    
    public static ArrayList<Ingredient> getProductIngredientData(Integer id) {
        DefaultIngredientQuantity[] defaultIngredientArray;
        ArrayList<Ingredient> ingredientList = new ArrayList<>();

        defaultIngredientArray = (DefaultIngredientQuantity[]) getData("Product/" + id + "/Ingredients/Default", DefaultIngredientQuantity[].class);
            
        for (int i = 0; i < defaultIngredientArray.length; i++) {
            ingredientList.add(getIngredientData(defaultIngredientArray[i].getIngredientId()));
            ingredientList.get(i).setQuantity(defaultIngredientArray[i].getQuantity());
        }

        return ingredientList;
    }
    
    public static ArrayList<Staff> getStaffData() {
        Staff[] staffArray;
        ArrayList<Staff> staffList = new ArrayList<>();

        staffArray = (Staff[]) getData("Staff", Staff[].class);
            
        for (Staff currentStaff : staffArray) {
            currentStaff.convertDate(currentStaff.getDob());
            staffList.add(currentStaff);
        }
        
        return staffList;
    }
    
    public static Staff getStaffData(Integer id) {
        Staff staff = (Staff) getData("Staff", Staff.class, id);
            
        staff.convertDate(staff.getDob());
        
        return staff;
    }
    
    public static String getStaffSalt(String username) {
        return (String) getData("Staff/" + username + "/Salt", String[].class)[0];
    }
    
    public static String getStaffPosition(String username) {
        return (String) getData("Staff/" + username + "/Position", String[].class)[0];
    }
    
    public static ArrayList<Store> getStoreData() {
        Store[] storeArray;
        ArrayList<Store> storeList = new ArrayList<>();
        
        storeArray = (Store[]) getData("Store", Store[].class);
        
        storeList.addAll(Arrays.asList(storeArray));
        
        return storeList;
    }
    
    public static Store getStoreData(Integer id) {
        return (Store) getData("Store", Store.class, id);
    }
    
    // PUT REQUESTS
    
    public static Boolean updateCustomerData(Customer customer) {
        Integer responseCode;
        
        responseCode = putData("Customer/" + customer.getCustomerId(), customer);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateDealData(Deal deal) {
        Integer responseCode;
        
        responseCode = putData("Deal/PutAsWhole", deal);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateDealItemData(DealItem dealItem) {
        Integer responseCode;
        
        responseCode = putData("DealItem/" + dealItem.getDealItemId(), dealItem);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateDealOrderItemData(DealOrderItem dealOrderItem) {
        Integer responseCode;
        
        responseCode = putData("DealOrderItem/" + dealOrderItem.getDealOrderItemId(), dealOrderItem);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateDefaultIngredientData(DefaultIngredientQuantity defaultIngredient) {
        Integer responseCode;
        
        responseCode = putData("DefaultIngredientQuantity/" + defaultIngredient.getProductId(), defaultIngredient);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateIngredientData(Ingredient ingredient) {
        Integer responseCode;
        
        responseCode = putData("Ingredient/" + ingredient.getIngredientId(), ingredient);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateOrderData(Order order) {
        Integer responseCode;
        
        responseCode = putData("Order/PutAsWhole", order);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateOrderItemData(OrderItem orderItem) {
        Integer responseCode;
        
        responseCode = putData("OrderItem/" + orderItem.getOrderItemId(), orderItem);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateOrderItemIngredientData(OrderItemIngredientQuantity orderItemIngredient) {
        Integer responseCode;
        
        responseCode = putData("DefaultIngredientQuantity/" + orderItemIngredient.getOrderItemId(), orderItemIngredient);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updatePaymentData(Payment payment) {
        Integer responseCode;
        
        responseCode = putData("Payment/" + payment.getPaymentId(), payment);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateProductData(Product product) {
        Integer responseCode;
        
        responseCode = putData("Product/PutAsWhole", product);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateProductTypeData(ProductType productType) {
        Integer response;
        
        response = putData("ProductType", productType);
        
        if ((response >= 200) && (response < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateProductSizesData(ProductSizes productSizes) {
        Integer responseCode;
        
        responseCode = putData("ProductSizes/" + productSizes.getProductId(), productSizes);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateSizeOptionsData(SizeOptions sizeOptions) {
        Integer responseCode;
        
        responseCode = putData("SizeOptions/" + sizeOptions.getSizeId(), sizeOptions);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateStaffData(Staff staff) {
        Integer responseCode;
        
        responseCode = putData("Staff/" + staff.getStaffId(), staff);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean updateStoreData(Store store) {
        Integer responseCode;
        
        responseCode = putData("Store/" + store.getStoreId(), store);
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    // POST REQUESTS
    
    public static Integer postCustomerData(Customer customer) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("Customer", customer);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return Integer.parseInt(response.get("responseMessage").toString());
        }
        
        return null;
    }
    
    public static Integer postDealData(Deal deal) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("Deal/PostAsWhole", deal);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return Integer.parseInt(response.get("responseMessage").toString());
        }
        
        return null;
    }
    
    public static Boolean postDealItemData(DealItem dealItem) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("DealItem", dealItem);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean postDealOrderItemData(DealOrderItem dealOrderItem) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("DealOrderItem", dealOrderItem);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean postDefaultIngredientData(DefaultIngredientQuantity defaultIngredient) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("DefaultIngredientQuantity", defaultIngredient);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Integer postIngredientData(Ingredient ingredient) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("Ingredient", ingredient);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return Integer.parseInt(response.get("responseMessage").toString());
        }
        
        return null;
    }
    
    public static Integer postOrderData(Order order) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("Order/PostAsWhole", order);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return Integer.parseInt(response.get("responseMessage").toString());
        }
        
        return null;
    }
    
    public static Boolean postOrderItemData(OrderItem orderItem) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("OrderItem", orderItem);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean postOrderItemIngredientData(OrderItemIngredientQuantity orderItemIngredient) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("DefaultIngredientQuantity", orderItemIngredient);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Integer postPaymentData(Payment payment) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("Payment", payment);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return Integer.parseInt(response.get("responseMessage").toString());
        }
        
        return null;
    }
    
    public static Integer postProductData(Product product) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("Product/PostAsWhole", product);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return Integer.parseInt(response.get("responseMessage").toString());
        }
        
        return null;
    }

    public static Boolean postProductTypeData(ProductType productType) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("ProductType", productType);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean postProductSizesData(ProductSizes productSizes) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("ProductSizes", productSizes);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean postSizeOptionsData(SizeOptions sizeOptions) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("SizeOptions", sizeOptions);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Integer postStaffData(Staff staff) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("Staff", staff);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return Integer.parseInt(response.get("responseMessage").toString());
        }
        
        return null;
    }
    
    public static Integer postStoreData(Store store) {
        HashMap<String, Object> response;
        
        Integer responseCode;
        
        response = postData("Store", store);
        
        responseCode = (Integer) response.get("responseCode");
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return Integer.parseInt(response.get("responseMessage").toString());
        }
        
        return null;
    }
    
    // DELETE REQUESTS
    
    public static Boolean deleteCustomerData(Customer customer) {
        Integer responseCode;
        
        responseCode = deleteData("Customer/" + customer.getCustomerId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteDealData(Deal deal) {
        Integer responseCode;
        
        responseCode = deleteData("Deal/" + deal.getDealId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteDealItemData(DealItem dealItem) {
        Integer responseCode;
        
        responseCode = deleteData("DealItem/" + dealItem.getDealItemId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteDealOrderItemData(DealOrderItem dealOrderItem) {
        Integer responseCode;
        
        responseCode = deleteData("DealOrderItem/" + dealOrderItem.getDealOrderItemId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteDefaultIngredientData(DefaultIngredientQuantity defaultIngredient) {
        Integer responseCode;
        
        responseCode = deleteData("DefaultIngredientQuantity/" + defaultIngredient.getProductId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteIngredientData(Ingredient ingredient) {
        Integer responseCode;
        
        responseCode = deleteData("Ingredient/" + ingredient.getIngredientId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteOrderData(Order order) {
        Integer responseCode;
        
        responseCode = deleteData("Order/" + order.getOrderId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteOrderItemData(OrderItem orderItem) {
        Integer responseCode;
        
        responseCode = deleteData("OrderItem/" + orderItem.getOrderItemId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteOrderItemIngredientData(OrderItemIngredientQuantity orderItemIngredient) {
        Integer responseCode;
        
        responseCode = deleteData("DefaultIngredientQuantity/" + orderItemIngredient.getOrderItemId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deletePaymentData(Payment payment) {
        Integer responseCode;
        
        responseCode = deleteData("Payment/" + payment.getPaymentId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteProductData(Product product) {
        Integer responseCode;
        
        responseCode = deleteData("Product/" + product.getProductId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteProductSizesData(ProductSizes productSizes) {
        Integer responseCode;
        
        responseCode = deleteData("ProductSizes/" + productSizes.getProductId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteSizeOptionsData(SizeOptions sizeOptions) {
        Integer responseCode;
        
        responseCode = deleteData("SizeOptions/" + sizeOptions.getSizeId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteStaffData(Staff staff) {
        Integer responseCode;
        
        responseCode = deleteData("Staff/" + staff.getStaffId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
    
    public static Boolean deleteStoreData(Store store) {
        Integer responseCode;
        
        responseCode = deleteData("Store/" + store.getStoreId());
        
        if ((responseCode >= 200) && (responseCode < 300)) {
            return true;
        }
        
        return false;
    }
}